@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
  <div class="col-md-8">
    <div class="card">
      <div class="card-header">{{ __('Register') }}</div>

      <div class="card-body">
        <form method="POST" action="" enctype="multipart/form-data">
          @csrf

          <div class="form-group row">
            <label for="name" class="col-md-4 col-form-label text-md-right">{{ ('Name') }}</label>
            <div class="col-md-6">
              <input id="name" type="text" class="form-control" name="name" value="" autocomplete="name" autofocus>
            </div>
          </div>

          <div class="form-group row">
            <label for="email" class="col-md-4 col-form-label text-md-right">{{ ('E-Mail Address') }}</label>

            <div class="col-md-6">
              <input id="email" type="email" class="form-control" name="email" value="" autocomplete="email">
            </div>
          </div>

          <div class="form-group row">
            <label for="address" class="col-md-4 col-form-label text-md-right">{{ ('Direccion') }}</label>

            <div class="col-md-6">
              <input id="address" type="text" class="form-control" name="address" value="{{ old('address') }}">
            </div>
          </div>

          <div class="form-group row">
            <label for="password" class="col-md-4 col-form-label text-md-right">{{ ('Password') }}</label>

            <div class="col-md-6">
              <input id="password" type="password" class="form-control" name="password">
            </div>
          </div>

          <div class="form-group row">
            <label for="password_confirmation" class="col-md-4 col-form-label text-md-right">{{ ('Confirm Password') }}</label>

            <div class="col-md-6">
              <input id="password_confirmation" type="password" class="form-control" name="password_confirmation">
            </div>
          </div>

          <div class="form-group row">
            <label for="photo" class="col-md-4 col-form-label text-md-right">{{ ('Foto') }}</label>

            <div class="col-md-6">
              <input id="photo" type="file" class="form-control" name="photo">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-6">
              <input id="name" type="hidden">
              <span class="invalid-feedback" role="alert">
                <!--TODO: Mensajes de errores de formulario-->
                @error('name')
                  <strong>El nombre es obligatorio</strong>
                @enderror
                @error('email')
                  <strong>El email es obligatorio</strong>
                @enderror
                @error('address')
                  <strong>La direccion es obligatoria</strong>
                @enderror
                @error('password')
                  <strong>La contraseña es obligatoria</strong>
                @enderror
                @error('password_confirmation')
                  <strong>No coinciden las contraseñas</strong>
                @enderror
                @error('photo')
                  <strong>La foto es obligatoria</strong>
                @enderror
                <!--Fin del Bloque-->
              </span>
            </div>
          </div>
          <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
              <button type="submit" class="btn btn-primary">
                {{ __('Pagar') }}
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<a href="{{ url('/compra/resumen') }}" class="btn btn-secondary btn-lg float-left">Atras</a>
<!--<a href="{{ url('/compra/confirmar') }}"  class="btn btn-primary btn-lg float-right">Siguiente</a>-->

<br><br>
@endsection