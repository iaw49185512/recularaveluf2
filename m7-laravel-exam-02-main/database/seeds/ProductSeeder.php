<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert(['name' => 'Peli 1', 'category' => 1, 'descripcion' => 'Esto es la descripcion pelicula 1', 'price' => 10.02, 'stock'=>6, 'image' => 'img01.jpg']);
        DB::table('products')->insert(['name' => 'Peli 2', 'category' => 2, 'descripcion' => 'Esto es la descripcion pelicula 2', 'price' => 11.02, 'stock'=>2, 'image' => 'img02.jpg']);
        DB::table('products')->insert(['name' => 'Peli 3', 'category' => 3, 'descripcion' => 'Esto es la descripcion pelicula 3', 'price' => 12.02, 'stock'=>3, 'image' => 'img03.jpg']);
        DB::table('products')->insert(['name' => 'Peli 4', 'category' => 3, 'descripcion' => 'Esto es la descripcion pelicula 4', 'price' => 13.02, 'stock'=>4, 'image' => 'img04.jpg']);

    
    
    }
}
